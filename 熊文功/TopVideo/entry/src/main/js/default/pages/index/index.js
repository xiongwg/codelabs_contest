export default {
    data: {
        videoList: [{
                        share_url: '/common/media/hm_sample_pv.mp4',
                        item_cover: '/common/images/bg-tv.jpg',
                        comments: [{
                                       userName: "ohos", comment: "hello,HDC2021!"
                                   },
                                   {
                                       userName: "ohos2", comment: "我期待HarmonyOS越来越棒！"
                                   }]
                    }, {
                        share_url: '/common/media/hm_sample_pv2.mp4',
                        item_cover: '/common/images/bg-tv.jpg',
                        comments: [{
                                       userName: "x", comment: "2021 HDC"
                                   },
                                   {
                                       userName: "yy", comment: "nice"
                                   }]
                    }, {
                        comments: [{
                                       userName: "ww", comment: "cool"
                                   },
                                   {
                                       userName: "dd", comment: "have a nice day! "
                                   }],
                        share_url: '/common/media/hm_sample_pv3.mp4',
                        item_cover: 'https://p3.douyinpic.com/img/tos-cn-p-0015/9aef497f80a14548a09273692bcba731_1633687064~c5_300x400.jpeg?from=2563711402_large'
                    }],
        isStart: true,
        playIndex: 0,
        showComment: false,
        commentInput: ''
    },
    onSwipe(obj) {
        console.log('xwg==> onSwipe obj = ' + JSON.stringify(obj))
        this.isStart = false;
        this.$element(`videoId${this.playIndex}`).pause();
        this.playIndex = obj.index
        this.showComment = false
        this.commentInput = ''

        this.$element(`videoId${this.playIndex}`).start();
        this.isStart = true;

    },
    showCommentPanel() {
        this.showComment = true
    },
    change_start_pause: function () {
        if (this.showComment) {
            this.showComment = false
            return
        }

        if (this.isStart) {
            this.$element(`videoId${this.playIndex}`).pause();
            this.isStart = false;
        } else {
            this.$element(`videoId${this.playIndex}`).start();
            this.isStart = true;
        }
    },
    commentInputChange(e) {
        this.commentInput = e.value
    },
    onCommentSend() {
        this.videoList[this.playIndex].comments.push({
            userName: '我', comment: this.commentInput
        })
        this.commentInput = ''
    },
    onBackPress() {
        if (this.showComment) {
            this.showComment = false
            return true
        }
        return false
    },
    transfer() {
        this.tryContinueAbility()
    },
    // shareData的数据会在onSaveData触发时与saveData一起传送到迁移目标FA，并绑定到其shareData数据段上
    // shareData的数据可以直接使用this访问。eg:this.remoteShareData1
    shareData: {},
    tryContinueAbility: async function () {
        // 应用进行迁移
        let result = await FeatureAbility.continueAbility();
        console.info("====result:" + JSON.stringify(result));
    },
    onStartContinuation() {
        // 判断当前的状态是不是适合迁移
        console.info("====onStartContinuation");
        return true;
    },
    onCompleteContinuation(code) {
        // 迁移操作完成，code返回结果
        console.info("=====CompleteContinuation: code = " + code);
    },
    onSaveData(saveData) {
        // 数据保存到savedData中进行迁移。
        var data = {
            videoList: this.videoList,
            playIndex: this.playIndex,
            showComment: this.showComment,
            isStart: this.isStart,
            commentInput: this.commentInput
        };
        Object.assign(saveData, data)
    },
    onRestoreData(restoreData) {
        // 收到迁移数据，恢复。
        console.info('==== onRestoreData ' + JSON.stringify(restoreData))
        this.videoList = restoreData.videoList
        this.playIndex = restoreData.playIndex

        this.$element('swiper').swipeTo({
            index: this.playIndex
        });
        this.showComment = restoreData.showComment
        this.isStart = restoreData.isStart
        this.commentInput = restoreData.commentInput
        if (this.isStart) {
            this.$element(`videoId${this.playIndex}`).start();
        } else {
            this.$element(`videoId${this.playIndex}`).pause();
        }
    }
}
