## JS仿抖音视频demo

## 项目介绍
* 功能：主要基于JS API 中[video 视频播放组件](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-components-media-video-0000000000611764)以及[swiper滑动容器组件](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-components-container-swiper-0000000000611533)实现类似抖音上下滑动切换视频播放功能。同时，依据鸿蒙[分布式迁移](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-apis-distributed-migration-0000001050024965)能力，实现了设备间视频流转功能。
* 开发版本：sdk6，DevEco Studio 2.2 Beta1

## 效果演示
  ![](img/swiper-video.gif)


 ## 实现过程分析
官方提供的Video Player Ability模板功能很强大，不仅实现了视频播放、分布式迁移，还实现了手机充当遥控器功能。有兴趣的可自行查看。

没有该模板的可自行下载，步骤如下：

1、在创建项目时，点击Template Market

![](img/videoTempalte1.png)

2、在搜索栏搜索“Video Player‘关键字，点击”download“下载模板

![](img/videoTempalte2.png)

3、下载完成后，选择“Video Player Ability”模板创建工程即可

![](img/videoTempalte3.png)



### 本项目大致分为三步实现：

#### 一、 实现上下滑动切换视频功能

官方给我们提供了[swiper](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-components-container-swiper-0000000000611533)组件，通过简单的配置即可实现上下切换子组件功能，[video](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-components-media-video-0000000000611764)组件很强大，但使用也比较简单。直接上代码：

```html
<div class="container">
    <swiper class="swiper" id="swiper" index="{{ playIndex }}" indicator="false"
            loop="true" digital="false" vertical="true" onchange="onSwipe">
            <video id='videoId{{ $idx }}' src='{{ $item.share_url }}' muted='false'
                   autoplay='true' poster=''
                   controls="false" onprepared='preparedCallback'
                   onstart='startCallback' onpause='pauseCallback'
                   onfinish='finishCallback' onerror='errorCallback'
                   onseeking='seekingCallback' onseeked='seekedCallback'
                   ontimeupdate='timeupdateCallback'
                   style="object-fit : scale-down; width : 100%; height : 100%;"
                   onlongpress='change_fullscreenchange' onclick="change_start_pause"
                   loop='true' starttime='0'></video>
        </stack>
    </swiper>
</div>
```

```js
export default {
    data: {
        videoList: [{
                        share_url: '/common/media/hm_sample_pv.mp4',
                        item_cover: '/common/images/bg-tv.jpg'
                    }, {
                        share_url: '/common/media/hm_sample_pv2.mp4',
                        item_cover: '/common/images/bg-tv.jpg'
                    }, {
                        share_url: '/common/media/hm_sample_pv3.mp4',
                        item_cover: '/common/images/bg-tv.jpg'
                    }],
        isStart: true,
        playIndex: 0,
      
    },
    onSwipe(obj) {
        console.log('xwg==> onSwipe obj = ' + JSON.stringify(obj))
        this.isStart = false;
        this.$element(`videoId${this.playIndex}`).pause();
     
        this.$element(`videoId${this.playIndex}`).start();
        this.isStart = true;

    }
}
```

很简单就实现了上下滑动切换播放功能

#### 二、添加播放暂停、评论功能

```html
<div class="container">
    <swiper class="swiper" id="swiper" index="{{ playIndex }}" indicator="false"
            loop="true" digital="false" vertical="true" onchange="onSwipe">
        <stack class="swiperContent" for="videoList">
            <video id='videoId{{ $idx }}' src='{{ $item.share_url }}' muted='false'
                   autoplay='true' poster=''
                   controls="false" onprepared='preparedCallback'
                   onstart='startCallback' onpause='pauseCallback'
                   onfinish='finishCallback' onerror='errorCallback'
                   onseeking='seekingCallback' onseeked='seekedCallback'
                   ontimeupdate='timeupdateCallback'
                   style="object-fit : scale-down; width : 100%; height : 100%;"
                   onlongpress='change_fullscreenchange' onclick="change_start_pause"
                   loop='true' starttime='0'></video>
            <image if="{{ playIndex != $idx }}"
                   src="{{ $item.item_cover }}" style="object-fit : scale-down;"></image>
            <div class="panel" onclick="change_start_pause">
                <image if="{{ playIndex != $idx || ! isStart }}" src="/common/images/icon_play.png"
                       class="playBtn"></image>
           
                <image src="/common/images/comment.png"
                       class="commentBtn" onclick="showCommentPanel"></image>
            </div>
        </stack>
    </swiper>
    <div class="commentPanel" if="{{ showComment }}">
        <list>
            <list-item for="{{ videoList[playIndex].comments }}">
                <text style="color : royalblue;">{{ $item.userName }} :</text>
                <text>{{ $item.comment }}</text>
            </list-item>
        </list>
        <div>
            <input type="text" value="{{ commentInput }}" placeholder="请输入" onchange="commentInputChange"></input>
            <button type="text"
                    onclick="onCommentSend"
                    style="padding-left : 15px; padding-right : 15px; width : 80px;">发送</button>
        </div>
    </div>
</div>
```

```js
export default {
    data: {
        videoList: [{
                        share_url: '/common/media/hm_sample_pv.mp4',
                        item_cover: '/common/images/bg-tv.jpg',
                        comments: [{
                                       userName: "ohos", comment: "hello,HDC2021!"
                                   },
                                   {
                                       userName: "ohos2", comment: "我期待HarmonyOS越来越棒！"
                                   }]
                    }, {
                        share_url: '/common/media/hm_sample_pv2.mp4',
                        item_cover: '/common/images/bg-tv.jpg',
                        comments: [{
                                       userName: "x", comment: "2021 HDC"
                                   },
                                   {
                                       userName: "yy", comment: "nice"
                                   }]
                    }, {
                        comments: [{
                                       userName: "ww", comment: "cool"
                                   },
                                   {
                                       userName: "dd", comment: "have a nice day! "
                                   }],
                        share_url: '/common/media/hm_sample_pv3.mp4',
                        item_cover: '/common/images/bg-tv.jpg'
                    }],
        isStart: true,
        playIndex: 0,
        showComment: false,
        commentInput: ''
    },
    onSwipe(obj) {
        console.log('xwg==> onSwipe obj = ' + JSON.stringify(obj))
        this.isStart = false;
        this.$element(`videoId${this.playIndex}`).pause();
        this.playIndex = obj.index
        this.showComment = false
        this.commentInput = ''

        this.$element(`videoId${this.playIndex}`).start();
        this.isStart = true;

    },
    showCommentPanel() {
        this.showComment = true
    },
    change_start_pause: function () {
        if (this.showComment) {
            this.showComment = false
            return
        }

        if (this.isStart) {
            this.$element(`videoId${this.playIndex}`).pause();
            this.isStart = false;
        } else {
            this.$element(`videoId${this.playIndex}`).start();
            this.isStart = true;
        }
    },
    commentInputChange(e) {
        this.commentInput = e.value
    },
    onCommentSend() {
        this.videoList[this.playIndex].comments.push({
            userName: '我', comment: this.commentInput
        })
        this.commentInput = ''
    },
    onBackPress() {
        if (this.showComment) {
            this.showComment = false
            return true
        }
        return false
    }
}
```

#### 三、实现流转功能

流转这块儿也没啥说的，按照文档来就是。重要的是处理下onSaveData和onRestoreData方法就好，另外就是流转需要申请权限

```js
transfer() {
    this.tryContinueAbility()
},
// shareData的数据会在onSaveData触发时与saveData一起传送到迁移目标FA，并绑定到其shareData数据段上
// shareData的数据可以直接使用this访问。eg:this.remoteShareData1
shareData: {},
tryContinueAbility: async function () {
    // 应用进行迁移
    let result = await FeatureAbility.continueAbility();
    console.info("====result:" + JSON.stringify(result));
},
onStartContinuation() {
    // 判断当前的状态是不是适合迁移
    console.info("====onStartContinuation");
    return true;
},
onCompleteContinuation(code) {
    // 迁移操作完成，code返回结果
    console.info("=====CompleteContinuation: code = " + code);
},
onSaveData(saveData) {
    // 数据保存到savedData中进行迁移。
    var data = {
        videoList: this.videoList,
        playIndex: this.playIndex,
        showComment: this.showComment,
        isStart: this.isStart,
        commentInput: this.commentInput
    };
    Object.assign(saveData, data)
},
onRestoreData(restoreData) {
    // 收到迁移数据，恢复。
    console.info('==== onRestoreData ' + JSON.stringify(restoreData))
    this.videoList = restoreData.videoList
    this.playIndex = restoreData.playIndex

    this.$element('swiper').swipeTo({
        index: this.playIndex
    });
    this.showComment = restoreData.showComment
    this.isStart = restoreData.isStart
    this.commentInput = restoreData.commentInput
    if (this.isStart) {
        this.$element(`videoId${this.playIndex}`).start();
    } else {
        this.$element(`videoId${this.playIndex}`).pause();
    }
}
```

动态权限的申请：

```json
// config.json
"reqPermissions": [
      {
        "reason": "",
        "usedScene": {
          "ability": [
            "MainAbility"
          ],
          "when": "inuse"
        },
        "name": "ohos.permission.DISTRIBUTED_DATASYNC"
      }
    ]
```

```java
// MainAbility.java
@Override
public void onStart(Intent intent) {
    requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"},0);
    super.onStart(intent);
}
```

###  完整代码

下载链接：[swiper-video]( https://gitee.com/xiongwg/swiper-video.git)

